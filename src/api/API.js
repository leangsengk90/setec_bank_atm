import axios from "axios";

export const API = axios.create({
  baseURL: "https://atm.plusmalls.com/api/",
//   withCredentials: false,
  headers: {
    "Accept": "application/json",
    "Content-Type": "application/json",
    "Access-Control-Allow-Origin": "*",
    "Access-Control-Allow-Methods": "GET,PUT,POST,DELETE,PATCH,OPTIONS",
  },
});
