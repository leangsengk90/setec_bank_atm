import React, { useContext } from "react";
import { Button } from "react-bootstrap";
import { useHistory } from "react-router-dom";
import { myContext } from "../App";

export default function MainMenu() {
  const history = useHistory();
  const {user} = useContext(myContext);

  return (
    <div>
      <Button
        style={{ width: "400px" }}
        variant="success"
        className="mt-5 text-center"
        onClick={()=>history.push("/")}
      >
        <table style={{marginRight: '20px'}}>
          <tr>
            <td style={{padding: '0'}}>
              <img style={{marginTop: '-4px'}} width="20px" src="/images/back2.png" />
            </td>
            <td style={{width: '100%',padding: '0'}}>
              <span>{user.username}</span>
            </td>
          </tr>
        </table>
      </Button>
      <div
        className="d-flex flex-row flex-wrap justify-content-center"
        style={{ margin: "20px 260px" }}
      >
        <Button onClick={()=> history.push('/menu/checkbalance')} variant="outline-success" style={myStyles.item}>
          <img width="36px" src="/images/checkbalance.png" />
          <h6 className="mt-3">Check Balance</h6>
        </Button>
        <Button onClick={()=> history.push('/menu/cashdeposit')} variant="outline-success" style={myStyles.item}>
          <img width="40px" src="/images/deposit.png" />
          <h6 className="mt-3">Cash Deposit</h6>
        </Button>
        <Button onClick={()=> history.push('/menu/cashwithdrawal')} variant="outline-success" style={myStyles.item}>
          <img width="40px" src="/images/withdrawal.png" />
          <h6 className="mt-3">Cash Withdrawal</h6>
        </Button>
        <Button onClick={()=> history.push('/menu/cashtransfer')} variant="outline-success" style={myStyles.item}>
          <img width="40px" src="/images/transfer.png" />
          <h6 className="mt-3">Cash Transfer</h6>
        </Button>
        <Button onClick={()=> history.push('/menu/billpayment')} variant="outline-success" style={myStyles.item}>
          <img width="40px" src="/images/payment.png" />
          <h6 className="mt-3">Bill Payment</h6>
        </Button>
        {/* <Button onClick={()=> history.push('/menu/transactionreport')} variant="outline-success" style={myStyles.item}>
          <img width="40px" src="/images/report.png" />
          <h6 className="mt-3">Transition Report</h6>
        </Button> */}
        <Button onClick={()=> history.push('/menu/changepassword')} variant="outline-success" style={myStyles.item}>
          <img width="50px" src="/images/lock.png" />
          <h6 className="mt-3">Change Password</h6>
        </Button>
        {/* <Button onClick={()=> history.push('/menu/exit')} variant="outline-success" style={myStyles.item}>
          <img width="34px" src="/images/exit.png" />
          <h6 className="mt-3">Exit</h6>
        </Button> */}
      </div>
    </div>
  );
}

const myStyles = {
  item: {
    height: "200px",
    width: "200px",
    borderColor: "lightgray",
    borderWidth: "1px",
    display: "flex",
    flexDirection: "column",
    justifyContent: "center",
    alignItems: "center",
    margin: "4px",
  },
};
