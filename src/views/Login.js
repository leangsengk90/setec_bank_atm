import axios from "axios";
import React, { useContext, useEffect, useState } from "react";
import { DropdownButton, Dropdown, Form, Button } from "react-bootstrap";
import { useHistory } from "react-router-dom";
import Swal from "sweetalert2";
import { API } from "../api/API";
import { myContext } from "../App";

export default function Login() {
  const [username, setUsername] = useState("");
  const [password, setPassword] = useState("");
  const history = useHistory();
  const { lang, setUser } = useContext(myContext);
  // const {welcome, index} = lang

  useEffect(() => {
    // console.log("CONTEXT:", context);
  }, []);

  function onLogin(e) {
    e.preventDefault();
    if (username == "" || password == "") {
      Swal.fire({
        title: "Empty Fields",
        text: "Please complete all the fields!",
        icon: "warning",
        showCancelButton: true,
        confirmButtonColor: "#3085d6",
        cancelButtonColor: "#d33",
        confirmButtonText: "OK",
      }).then((result) => {
        // if (result.isConfirmed) {
        //   Swal.fire("OK", "Your file has been deleted.", "success");
        // }
      });
    } else {
      API.post(`sign-in?username=${username}&password=${password}`)
        .then((result) => {
          console.log("RESULT:", result);
          localStorage.setItem(
            "user",
            JSON.stringify({
              username: `${result.data.data.username}`,
              token: `${result.data.data.token}`,
            })
          );
          setUser((curr) => ({ ...curr, username }));
          history.push("/menu");
        })
        .catch((error) => {
          console.log("ERROR:", error);
          Swal.fire({
            title: "Username or Password not Match",
            text: "Please check username and password again!",
            icon: "warning",
            showCancelButton: true,
            confirmButtonColor: "#3085d6",
            cancelButtonColor: "#d33",
            confirmButtonText: "OK",
          }).then((result) => {
            // if (result.isConfirmed) {
            //   Swal.fire("OK", "Your file has been deleted.", "success");
            // }
          });
        });
    }
  }

  return (
    <div style={{ marginTop: "20px" }}>
      <h3>{lang.welcome[lang.lidx]}</h3>
      {/* <span>ROLE </span> */}
      {/* <select style={{padding: '6px 36px', borderRadius: '9999px'}}>
          <option>Admin</option>
          <option>User</option>
      </select> */}
      <Form>
        {/* <Form.Group
          controlId="exampleForm.ControlSelect1"
          className="d-flex justify-content-center mx-auto"
          style={{ width: "200px" }}
        >
          <Form.Label className="mx-3 mt-2">ROLE </Form.Label>
          <Form.Control as="select">
            <option>Admin</option>
            <option>User</option>
          </Form.Control>
        </Form.Group> */}
        <Form.Group>
          {/* <Form.Label>Password</Form.Label> */}
          <Form.Control
            type="text"
            className="w-25 mx-auto"
            placeholder={`${lang.username[lang.lidx]}`}
            name="username"
            value={username}
            onChange={(e) => setUsername(e.target.value)}
          />
        </Form.Group>
        <Form.Group controlId="formBasicPassword">
          {/* <Form.Label>Password</Form.Label> */}
          <Form.Control
            type="password"
            className="w-25 mx-auto"
            placeholder={`${lang.password[lang.lidx]}`}
            name="password"
            value={password}
            onChange={(e) => setPassword(e.target.value)}
          />
        </Form.Group>
        {/* <Form.Group controlId="formBasicCheckbox">
          <Form.Check type="checkbox" label="Check me out" />
        </Form.Group> */}
        <Button onClick={onLogin} variant="primary" type="submit">
          {lang.signin[lang.lidx]}
        </Button>{" "}
        <Button
          onClick={() => history.push("/signup")}
          variant="warning"
          type="submit"
        >
          {lang.signup[lang.lidx]}
        </Button>
        <Form.Group className="mt-3">
          {/* <a href="#">Forget password?</a> */}
        </Form.Group>
      </Form>
    </div>
  );
}
