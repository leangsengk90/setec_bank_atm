import React, { useContext, useEffect, useState } from "react";
import { DropdownButton, Dropdown, Form, Button } from "react-bootstrap";
import { useHistory } from "react-router-dom";
import Swal from "sweetalert2";
import { API } from "../api/API";
import { myContext } from "../App";

export default function SignUp() {
  const [name, setName] = useState("");
  const [username, setUsername] = useState("")
  const [account_no, setAccount_no] = useState("")
  const [password, setPassword] = useState("")
  const { user, setUser } = useContext(myContext);
  const history = useHistory();

  function onSignup() {
    // e.preventDefult();
   
    if (
      name == "" ||
      username == "" ||
      account_no == "" ||
      password == ""
    ) {
      Swal.fire({
        title: "Empty Fields",
        text: "Please complete all the fields!",
        icon: "warning",
        showCancelButton: true,
        confirmButtonColor: "#3085d6",
        cancelButtonColor: "#d33",
        confirmButtonText: "OK",
      }).then((result) => {
        // if (result.isConfirmed) {
        //   Swal.fire("OK", "Your file has been deleted.", "success");
        // }
      });

    } else {
      API.post(
        `customers?name=${name}&account_no=${account_no}&username=${username}&password=${password}`
      )
        .then((result) => {
          console.log("RESULT", result);
          if(result.data.status == 'error'){
            Swal.fire({
              title: "Username or Password not Match",
              text: "Please check username and password again!",
              icon: "warning",
              showCancelButton: true,
              confirmButtonColor: "#3085d6",
              cancelButtonColor: "#d33",
              confirmButtonText: "OK",
            }).then((result) => {
              // if (result.isConfirmed) {
              //   Swal.fire("OK", "Your file has been deleted.", "success");
              // }
            });
          }else{
            setUser(curr=>({...curr, username}))
            history.push('/menu')
          }
        })
        .catch((error) => {
          console.log("ERROR:", error);
          Swal.fire({
            title: "Username or Password not Match",
            text: "Please check username and password again!",
            icon: "warning",
            showCancelButton: true,
            confirmButtonColor: "#3085d6",
            cancelButtonColor: "#d33",
            confirmButtonText: "OK",
          }).then((result) => {
            // if (result.isConfirmed) {
            //   Swal.fire("OK", "Your file has been deleted.", "success");
            // }
          });
        });
    }
  }

  return (
    <div style={{ marginTop: "20px" }}>
      <h3>Sign Up</h3>
      {/* <span>ROLE </span> */}
      {/* <select style={{padding: '6px 36px', borderRadius: '9999px'}}>
          <option>Admin</option>
          <option>User</option>
      </select> */}
      <Form>
        <Form.Group>
          {/* <Form.Label>Password</Form.Label> */}
          <Form.Control
            type="text"
            className="w-25 mx-auto"
            placeholder="Name"
            name="name"
            value={name}
            onChange={(e) => setName(e.target.value)}
          />
        </Form.Group>
        <Form.Group>
          {/* <Form.Label>Password</Form.Label> */}
          <Form.Control
            type="text"
            className="w-25 mx-auto"
            placeholder="Username"
            name="username"
            value={username}
            onChange={(e) => setUsername(e.target.value)}
          />
        </Form.Group>
        <Form.Group controlId="formBasicPassword">
          {/* <Form.Label>Password</Form.Label> */}
          <Form.Control
            type="password"
            className="w-25 mx-auto"
            placeholder="Account Number"
            name="account_no"
            value={account_no}
            onChange={(e) => setAccount_no(e.target.value)}
          />
        </Form.Group>
        <Form.Group controlId="formBasicPassword">
          {/* <Form.Label>Password</Form.Label> */}
          <Form.Control
            type="password"
            className="w-25 mx-auto"
            placeholder="Password"
            name="password"
            value={password}
            onChange={(e) => setPassword(e.target.value)}
          />
        </Form.Group>
        {/* <Form.Group controlId="formBasicCheckbox">
          <Form.Check type="checkbox" label="Check me out" />
        </Form.Group> */}
        <Button variant="primary" type="button" onClick={onSignup}>
          Sign up
        </Button>{" "}
        <Button
          onClick={() => history.push("/")}
          variant="warning"
          type="submit"
        >
          Cancel
        </Button>
      </Form>
    </div>
  );
}
