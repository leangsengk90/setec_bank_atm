import React from "react";
import { Button, Form, ButtonGroup, Table } from "react-bootstrap";
import { useHistory } from "react-router-dom";
import "../App.css";

export default function TransactionReport() {
  const history = useHistory();
  return (
    <div>
      <Button
        style={{ width: "400px" }}
        variant="success"
        className="mt-5 text-center"
        onClick={()=>history.push("/menu")}
      >
        <table style={{ marginRight: "20px" }}>
          <tr>
            <td style={{ padding: "0" }}>
              <img
                style={{ marginTop: "-4px" }}
                width="20px"
                src="/images/back2.png"
              />
            </td>
            <td style={{ width: "100%", padding: "0" }}>
              <span>Transaction Report</span>
            </td>
          </tr>
        </table>
      </Button>
      <div
        style={{
          //   border: "1px solid lightgray",
          padding: "20px 100px",
          //   width: "640px",
          margin: "20px auto",
        }}
      >
        <Form className="d-flex">
          <ButtonGroup
            vertical
            style={{ width: "300px", height: "60px", margin: "50px 20px 0 0" }}
          >
            <Button className="text-left" variant="outline-primary">
              <img
                width="26px"
                style={{ margin: "0 10px 0 20px" }}
                src="/images/deposit.png"
              />
              Cash Deposit
            </Button>
            <Button className="text-left" variant="outline-primary">
              <img
                width="26px"
                style={{ margin: "0 10px 0 20px" }}
                src="/images/withdrawal.png"
              />
              Cash Withdrawal
            </Button>
            <Button className="text-left" variant="outline-primary">
              <img
                width="26px"
                style={{ margin: "0 10px 0 20px" }}
                src="/images/transfer.png"
              />
              Cash Transfer
            </Button>
            <Button className="text-left" variant="outline-primary">
              <img
                width="26px"
                style={{ margin: "0 10px 0 20px" }}
                src="/images/payment.png"
              />
              Bill Payment
            </Button>
          </ButtonGroup>
          <div className="d-flex flex-sm-column w-100">
          <Table striped bordered hover size="sm">
            <thead>
              <tr>
                <th>#</th>
                <th>Cash Deposit</th>
                <th>Location</th>
                <th>Date</th>
                <th>Time</th>
              </tr>
            </thead>
            <tbody>
              <tr>
                <td>1</td>
                <td>100 $</td>
                <td>#setec-001</td>
                <td>01-01-2021</td>
                <td>10:12:13 AM</td>
              </tr>
            </tbody>
          </Table>
          <Table striped bordered hover size="sm">
            <thead>
              <tr>
                <th>#</th>
                <th>Cash Withdrawal</th>
                <th>Location</th>
                <th>Date</th>
                <th>Time</th>
              </tr>
            </thead>
            <tbody>
              <tr>
                <td>1</td>
                <td>100 $</td>
                <td>#setec-001</td>
                <td>01-01-2021</td>
                <td>10:12:13 AM</td>
              </tr>
        
            </tbody>
          </Table>
          <Table striped bordered hover size="sm">
            <thead>
              <tr>
                <th>#</th>
                <th>Receiver Name</th>
                <th>Receiver Number</th>
                <th>Cash Transfer</th>
                <th>Location</th>
                <th>Date</th>
                <th>Time</th>
              </tr>
            </thead>
            <tbody>
              <tr>
                <td>1</td>
                <td>Bro KH</td>
                <td>88889999</td>
                <td>100 $</td>
                <td>#setec-001</td>
                <td>01-01-2021</td>
                <td>10:12:13 AM</td>
              </tr>
            
            </tbody>
          </Table>
          <Table striped bordered hover size="sm">
            <thead>
              <tr>
                <th>#</th>
                <th>Bill ID</th>
                <th>Bill Name</th>
                <th>Amount</th>
                <th>Location</th>
                <th>Date</th>
                <th>Time</th>
              </tr>
            </thead>
            <tbody>
              <tr>
                <td>1</td>
                <td>P123</td>
                <td>Smart Top-up</td>
                <td>100 $</td>
                <td>#setec-001</td>
                <td>01-01-2021</td>
                <td>10:12:13 AM</td>
              </tr>
             
            </tbody>
          </Table>
          </div>
        </Form>
      </div>
    </div>
  );
}
