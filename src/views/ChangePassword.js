import React, { useState, useEffect } from "react";
import { Button, Form } from "react-bootstrap";
import { useHistory } from "react-router-dom";
import Swal from "sweetalert2";
import { API } from "../api/API";
import "../App.css";

export default function ChangePassword() {
  const [currentPass, setCurrentPass] = useState("");
  const [newPass, setNewPass] = useState("");
  const [username, setUserName] = useState("");
  const history = useHistory();
  const { token } = JSON.parse(localStorage.getItem("user"));

  function onUpdatePassword() {
    if (username == "" || newPass == "" || currentPass == "") {
      Swal.fire({
        title: "Empty Fields",
        text: "Please complete all the fields!",
        icon: "warning",
        showCancelButton: true,
        confirmButtonColor: "#3085d6",
        cancelButtonColor: "#d33",
        confirmButtonText: "OK",
      }).then((result) => {
        // if (result.isConfirmed) {
        //   Swal.fire("OK", "Your file has been deleted.", "success");
        // }
      });
    } else {
      API.put(
        `change-password?current_password=${currentPass}&new_password=${newPass}&confirm_password=${newPass}`,
        {},
        { headers: { Authorization: "Bearer " + token } }
      )
        .then((result) => {
          console.log("CHANGE PASSWORD RESULT:", result);
          Swal.fire({
            title: "Account Updated Successfully",
            text: "Your account has been updated successfully!",
            icon: "success",
            showCancelButton: true,
            confirmButtonColor: "#3085d6",
            cancelButtonColor: "#d33",
            confirmButtonText: "OK",
          }).then((result) => {
            // if (result.isConfirmed) {
            //   Swal.fire("OK", "Your file has been deleted.", "success");
            // }
          });
          setUserName("")
          setCurrentPass("")
          setNewPass("")
        })
        .catch((error) => {
          console.log("CHANGE PASSWORD ERROR:", error);
          Swal.fire({
            title: "Username or Password not Match!",
            text: "Please check username and password again!",
            icon: "warning",
            showCancelButton: true,
            confirmButtonColor: "#3085d6",
            cancelButtonColor: "#d33",
            confirmButtonText: "OK",
          }).then((result) => {
            // if (result.isConfirmed) {
            //   Swal.fire("OK", "Your file has been deleted.", "success");
            // }
          });
        });
    }
  }

  return (
    <div>
      <Button
        style={{ width: "400px" }}
        variant="success"
        className="mt-5 text-center"
        onClick={() => history.push("/menu")}
      >
        <table style={{ marginRight: "20px" }}>
          <tr>
            <td style={{ padding: "0" }}>
              <img
                style={{ marginTop: "-4px" }}
                width="20px"
                src="/images/back2.png"
              />
            </td>
            <td style={{ width: "100%", padding: "0" }}>
              <span>Change Password</span>
            </td>
          </tr>
        </table>
      </Button>
      <div
        style={{
          border: "1px solid lightgray",
          padding: "20px",
          width: "640px",
          margin: "20px auto",
        }}
      >
        <Form>
          <Form.Group>
            {/* <Form.Label>Password</Form.Label> */}
            <Form.Control
              type="text"
              className="w-50 mx-auto"
              placeholder="Username"
              name="username"
              value={username}
              onChange={(e) => setUserName(e.target.value)}
            />
          </Form.Group>
          <Form.Group controlId="formBasicPassword">
            {/* <Form.Label>Password</Form.Label> */}
            <Form.Control
              type="password"
              className="w-50 mx-auto"
              placeholder="Password"
              name="currentPass"
              value={currentPass}
              onChange={(e) => setCurrentPass(e.target.value)}
            />
          </Form.Group>
          <Form.Group controlId="formBasicPassword">
            {/* <Form.Label>Password</Form.Label> */}
            <Form.Control
              type="password"
              className="w-50 mx-auto"
              placeholder="New password"
              name="newPass"
              value={newPass}
              onChange={(e) => setNewPass(e.target.value)}
            />
          </Form.Group>
          {/* <Form.Group controlId="formBasicCheckbox">
          <Form.Check type="checkbox" label="Check me out" />
        </Form.Group> */}
          <Button variant="primary" type="button" onClick={onUpdatePassword}>
            Update password
          </Button>{" "}
          {/* <Button variant="warning" type="submit">
          Sign up
        </Button> */}
        </Form>
      </div>
    </div>
  );
}
