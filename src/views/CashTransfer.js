import React, { useState, useEffect } from "react";
import { Button, Form } from "react-bootstrap";
import { useHistory } from "react-router-dom";
import Swal from "sweetalert2";
import { API } from "../api/API";
import "../App.css";
import ModalReport from "../components/ModalReport";

export default function CashTransfer() {
  const [user, setUser] = useState({ name: "", account_no: "", balance: 0 });
  const [amount, setAmount] = useState(0);
  const [receiverAccount, setReceiverAccount] = useState("");
  const [receiverName, setReceiverName] = useState("");
  const [transactionDesc, setTransactionDesc] = useState("");
  const { token } = JSON.parse(localStorage.getItem("user"));
  const history = useHistory();
  const [isShow, setIsShow] = useState(false);
  const [titles, setTitles] = useState([
    "Sender Name",
    "Sender Number",
    "Receiver Name",
    "Receiver Number",
    "Amount",
    "Date",
  ]);

  const [descriptions, setDescriptions] = useState([
    "Kao Leangseng",
    "1234567890",
    "Bro KH",
    "88889999",
    10,
    "01-01-21",
  ]);

  function onIsShow() {
    let sub_balance = Number.parseFloat(user.balance) - Number.parseFloat(amount)
    if (receiverAccount == "" || receiverName == "" || amount == 0) {
      Swal.fire({
        title: "Empty Fields",
        text: "Please complete all the fields!",
        icon: "warning",
        showCancelButton: true,
        confirmButtonColor: "#3085d6",
        cancelButtonColor: "#d33",
        confirmButtonText: "OK",
      }).then((result) => {
        // if (result.isConfirmed) {
        //   Swal.fire("OK", "Your file has been deleted.", "success");
        // }
      });
    }else if(sub_balance < 0){
      Swal.fire({
        title: "Number invalid",
        text: "Your balance is insufficient!",
        icon: "warning",
        showCancelButton: true,
        confirmButtonColor: "#3085d6",
        cancelButtonColor: "#d33",
        confirmButtonText: "OK",
      }).then((result) => {
        // if (result.isConfirmed) {
        //   Swal.fire("OK", "Your file has been deleted.", "success");
        // }
      });
    } else {
      setDescriptions([
        user.name,
        user.account_no,
        receiverName,
        receiverAccount,
        amount,
        new Date().toLocaleString(),
      ]);
      setIsShow(true);
    }
  }

  function onYes() {
    API.post(
      `transfer?account_no=${receiverAccount}&balance=${amount}`,
      {},
      { headers: { Authorization: "Bearer " + token } }
    )
      .then((result) => {
        console.log("RESULT:", result);
        setUser((curr) => ({
          ...curr,
          balance: Number.parseFloat(curr.balance) - Number.parseFloat(amount),
        }));
        onClearAll();
      })
      .catch((error) => {
        console.log("ERROR:", error.responseText);
        Swal.fire({
          title: "Account not Match!",
          text: "Please input the correct account number!",
          icon: "warning",
          showCancelButton: true,
          confirmButtonColor: "#3085d6",
          cancelButtonColor: "#d33",
          confirmButtonText: "OK",
        }).then((result) => {
          // if (result.isConfirmed) {
          //   Swal.fire("OK", "Your file has been deleted.", "success");
          // }
        });
      });
    setIsShow(false);
  }

  function onIsClose() {
    setIsShow(false);
  }

  function onClearAll() {
    setAmount(0);
    setTransactionDesc("");
    setReceiverAccount("");
  }

  useEffect(() => {
    API.get("check-balance", { headers: { Authorization: "Bearer " + token } })
      .then((result) => {
        console.log("RESULT:", result);
        setUser({
          name: result.data.data.name,
          account_no: result.data.data.account_no,
          balance: result.data.data.balance,
        });
      })
      .catch((error) => {
        console.log("ERROR:", error);
      });
  }, [token]);

  return (
    <div>
      <Button
        style={{ width: "400px" }}
        variant="success"
        className="mt-5 text-center"
        onClick={() => history.push("/menu")}
      >
        <table style={{ marginRight: "20px" }}>
          <tr>
            <td style={{ padding: "0" }}>
              <img
                style={{ marginTop: "-4px" }}
                width="20px"
                src="/images/back2.png"
              />
            </td>
            <td style={{ width: "100%", padding: "0" }}>
              <span>Cash Transfer</span>
            </td>
          </tr>
        </table>
      </Button>
      <div
        style={{
          border: "1px solid lightgray",
          padding: "20px",
          width: "640px",
          margin: "20px auto",
        }}
      >
        <table
          style={{
            //   border: "1px solid lightgray",
            margin: "0 auto",
            //   marginTop: "20px",
            textAlign: "left",
          }}
        >
          <tr>
            <td>Account Name:</td>
            <td className="text-1">{user.name}</td>
            <td rowSpan="6" className="text-center">
              <Form.Group className="mb-3" controlId="formBasicPassword">
                {/* <Form.Label>Amount</Form.Label> */}
                <Form.Control
                  type="type"
                  placeholder="Account Name"
                  name="receiverName"
                  value={receiverName}
                  onChange={(e) => setReceiverName(e.target.value)}
                />
              </Form.Group>
              <Form.Group className="mb-3" controlId="formBasicPassword">
                {/* <Form.Label>Amount</Form.Label> */}
                <Form.Control
                  type="type"
                  placeholder="Receiver Account"
                  name="receiverAccount"
                  value={receiverAccount}
                  onChange={(e) => setReceiverAccount(e.target.value)}
                />
              </Form.Group>
              <Form.Group className="mb-3" controlId="formBasicPassword">
                {/* <Form.Label>Amount</Form.Label> */}
                <Form.Control
                  type="number"
                  placeholder="Amount"
                  min="0"
                  name="amount"
                  value={amount}
                  onChange={(e) => setAmount(e.target.value)}
                />
              </Form.Group>
              <Form.Group className="mb-3" controlId="formBasicPassword">
                {/* <Form.Label>Amount</Form.Label> */}
                <Form.Control
                  type="text"
                  placeholder="Description"
                  name="transactionDesc"
                  value={transactionDesc}
                  onChange={(e) => setTransactionDesc(e.target.value)}
                />
              </Form.Group>
              <Button onClick={onIsShow} variant="primary">
                Send
              </Button>{" "}
              <Button variant="warning" onClick={onClearAll}>
                Clear all
              </Button>
            </td>
            {/* <td rowSpan="6">Close</td> */}
          </tr>
          <tr>
            <td>Account Number:</td>
            <td className="text-1">{user.account_no}</td>
          </tr>
          <tr>
            <td>Your Balance:</td>
            <td className="text-1">{user.balance}</td>
          </tr>
          <tr>
            <td colSpan="2" className="text-center">
              <div className="balance mx-auto">
                <p className="inner-balance">{amount} $</p>
              </div>
              Cash Transfer
            </td>
          </tr>
        </table>
      </div>
      <ModalReport
        isShow={isShow}
        titles={titles}
        descriptions={descriptions}
        onIsClose={onIsClose}
        onYes={onYes}
      />
    </div>
  );
}
