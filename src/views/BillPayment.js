import React, { useEffect, useState } from "react";
import { Button, Form, ButtonGroup, Table } from "react-bootstrap";
import { useHistory } from "react-router-dom";
import Swal from "sweetalert2";
import { API } from "../api/API";
import "../App.css";
import ModalReport from "../components/ModalReport";

export default function BillPayment() {
  const [payment, setPayment] = useState(["phone", "electric", "water"]);
  const [amount, setAmount] = useState(0);
  const [phone, setPhone] = useState("");
  const { token } = JSON.parse(localStorage.getItem("user"));
  const [user, setUser] = useState({ name: "", account_no: "", balance: 0 });
  const [id, setId] = useState(0);
  const history = useHistory();
  const [isShow, setIsShow] = useState(false);
  const [titles, setTitles] = useState([
    "Account Name",
    "Account Number",
    `${payment[id]} Number`,
    "Amount",
    "Date",
  ]);

  const [descriptions, setDescriptions] = useState([
    "Kao Leangseng",
    "1234567890",
    "012346789",
    1,
    "01-01-21",
  ]);

  function onIsShow() {
    let sub_balance = Number.parseFloat(user.balance) - Number.parseFloat(amount)
    if (phone == "" || amount == 0) {
      Swal.fire({
        title: "Empty Fields",
        text: "Please complete all the fields!",
        icon: "warning",
        showCancelButton: true,
        confirmButtonColor: "#3085d6",
        cancelButtonColor: "#d33",
        confirmButtonText: "OK",
      }).then((result) => {
        // if (result.isConfirmed) {
        //   Swal.fire("OK", "Your file has been deleted.", "success");
        // }
      });
    }else if(sub_balance < 0){
      Swal.fire({
        title: "Number invalid",
        text: "Your balance is insufficient!",
        icon: "warning",
        showCancelButton: true,
        confirmButtonColor: "#3085d6",
        cancelButtonColor: "#d33",
        confirmButtonText: "OK",
      }).then((result) => {
        // if (result.isConfirmed) {
        //   Swal.fire("OK", "Your file has been deleted.", "success");
        // }
      });
    } else {
      setTitles([
        "Account Name",
        "Account Number",
        `${payment[id].charAt(0).toUpperCase() + payment[id].slice(1)} Number`,
        "Amount",
        "Date",
      ]);
      setDescriptions([
        user.name,
        user.account_no,
        phone,
        amount,
        new Date().toLocaleString(),
      ]);
      setIsShow(true);
    }
  }

  function onIsClose() {
    setIsShow(false);
  }

  function onYes() {
    API.post(
      `pay-billing?balance=${amount}&billing_no=${phone}&billing_type=${payment[id]}`,
      {},
      { headers: { Authorization: "Bearer " + token } }
    )
      .then((result) => {
        console.log("RESULT:", result);
        setUser((curr) => ({
          ...curr,
          balance: Number.parseFloat(curr.balance) - Number.parseFloat(amount),
        }));
        onClearAll();
      })
      .catch((error) => {
        console.log("ERROR:", error.responseText);
        Swal.fire({
          title: "Payment not Match!",
          text: "Please check your payment number!",
          icon: "warning",
          showCancelButton: true,
          confirmButtonColor: "#3085d6",
          cancelButtonColor: "#d33",
          confirmButtonText: "OK",
        }).then((result) => {
          // if (result.isConfirmed) {
          //   Swal.fire("OK", "Your file has been deleted.", "success");
          // }
        });
      });
    setIsShow(false);
  }

  function onClearAll() {
    setAmount(0);
    setPhone("");
  }

  useEffect(() => {
    setAmount(0)
    setPhone("")

    API.get("check-balance", { headers: { Authorization: "Bearer " + token } })
      .then((result) => {
        console.log("RESULT:", result);
        setUser({
          name: result.data.data.name,
          account_no: result.data.data.account_no,
          balance: result.data.data.balance,
        });
      })
      .catch((error) => {
        console.log("ERROR:", error);
      });
  }, [id]);

  return (
    <div>
      <Button
        style={{ width: "400px" }}
        variant="success"
        className="mt-5 text-center"
        onClick={() => history.push("/menu")}
      >
        <table style={{ marginRight: "20px" }}>
          <tr>
            <td style={{ padding: "0" }}>
              <img
                style={{ marginTop: "-4px" }}
                width="20px"
                src="/images/back2.png"
              />
            </td>
            <td style={{ width: "100%", padding: "0" }}>
              <span>Bill Payment</span>
            </td>
          </tr>
        </table>
      </Button>
      <div
        style={{
          //   border: "1px solid lightgray",
          padding: "20px 100px",
          //   width: "640px",
          margin: "20px auto",
        }}
      >
        <Form className="d-flex">
          <ButtonGroup
            vertical
            style={{ width: "300px", height: "60px", margin: "30px 20px 0 0" }}
          >
            <Button
              className={`text-left ${
                id == 0 ? "bg-primary text-white" : null
              }`}
              variant="outline-primary"
              id="0"
              onClick={(e) => setId(e.target.id)}
            >
              <img
                width="26px"
                style={{ margin: "0 10px 0 20px" }}
                src="/images/withdrawal.png"
              />
              Mobile Top-up
            </Button>
            <Button
              className={`text-left ${
                id == 1 ? "bg-primary text-white" : null
              }`}
              variant="outline-primary"
              id="1"
              onClick={(e) => setId(e.target.id)}
            >
              <img
                width="26px"
                style={{ margin: "0 10px 0 20px" }}
                src="/images/transfer.png"
              />
              Electricity Payment
            </Button>
            <Button
              className={`text-left ${
                id == 2 ? "bg-primary text-white" : null
              }`}
              variant="outline-primary"
              id="2"
              onClick={(e) => setId(e.target.id)}
            >
              <img
                width="26px"
                style={{ margin: "0 10px 0 20px" }}
                src="/images/payment.png"
              />
              Water Payment
            </Button>
          </ButtonGroup>
          <div className="d-flex flex-sm-column w-100">
            <div
              style={{
                border: "1px solid lightgray",
                padding: "20px",
                width: "100%",
                margin: "0px auto",
                display: `${id == 0 ? "inline" : "none"}`,
              }}
            >
              <table
                style={{
                  //   border: "1px solid lightgray",
                  margin: "0 auto",
                  //   marginTop: "20px",
                  textAlign: "left",
                }}
              >
                <tr>
                  <td>Account Name:</td>
                  <td className="text-1">{user.name}</td>
                  <td rowSpan="6" className="text-center">
                    <Button variant="warning" onClick={() => setAmount(5)}>
                      5 $
                    </Button>{" "}
                    <Button variant="warning" onClick={() => setAmount(10)}>
                      10 $
                    </Button>{" "}
                    <br />
                    <Button
                      className="my-2"
                      variant="warning"
                      onClick={() => setAmount(1)}
                    >
                      1 $
                    </Button>{" "}
                    <Button
                      className="my-2"
                      variant="warning"
                      onClick={() => setAmount(2)}
                    >
                      2 $
                    </Button>
                  </td>
                  {/* <td rowSpan="6">Close</td> */}
                </tr>
                <tr>
                  <td>Account Number:</td>
                  <td className="text-1">{user.account_no}</td>
                </tr>
                <tr>
                  <td>Your Balance:</td>
                  <td className="text-1">{user.balance}</td>
                </tr>
                <tr>
                  <td colSpan="2" className="text-center">
                    <Form.Group className="mb-3" controlId="formBasicPassword">
                      {/* <Form.Label>Amount</Form.Label> */}
                      <Form.Control
                        type="text"
                        placeholder="Phone number"
                        name="phone"
                        value={phone}
                        onChange={(e) => setPhone(e.target.value)}
                      />
                    </Form.Group>
                    <Form.Group className="mb-3" controlId="formBasicPassword">
                      {/* <Form.Label>Amount</Form.Label> */}
                      <Form.Control
                        type="number"
                        min="0"
                        placeholder="Amount"
                        name="amount"
                        value={amount}
                        onChange={(e) => setAmount(e.target.value)}
                      />
                    </Form.Group>
                    <Button onClick={onIsShow} variant="primary">
                      Confirm
                    </Button>{" "}
                    <Button variant="warning" onClick={onClearAll}>
                      Clear all
                    </Button>
                  </td>
                </tr>
              </table>
            </div>
            {/* </div>
          <div className="d-flex flex-sm-column w-100"> */}
            <div
              style={{
                border: "1px solid lightgray",
                padding: "20px",
                width: "100%",
                margin: "0px auto",
                display: `${id == 1 ? "inline" : "none"}`,
              }}
            >
              <table
                style={{
                  //   border: "1px solid lightgray",
                  margin: "0 auto",
                  //   marginTop: "20px",
                  textAlign: "left",
                }}
              >
                <tr>
                  <td>Account Name:</td>
                  <td className="text-1">{user.name}</td>
                  <td rowSpan="6" className="text-center">
                    <Button variant="warning" onClick={() => setAmount(5)}>
                      5 $
                    </Button>{" "}
                    <Button variant="warning" onClick={() => setAmount(10)}>
                      10 $
                    </Button>{" "}
                    <br />
                    <Button
                      className="my-2"
                      variant="warning"
                      onClick={() => setAmount(1)}
                    >
                      1 $
                    </Button>{" "}
                    <Button
                      className="my-2"
                      variant="warning"
                      onClick={() => setAmount(2)}
                    >
                      2 $
                    </Button>
                  </td>
                  {/* <td rowSpan="6">Close</td> */}
                </tr>
                <tr>
                  <td>Account Number:</td>
                  <td className="text-1">{user.account_no}</td>
                </tr>
                <tr>
                  <td>Your Balance:</td>
                  <td className="text-1">{user.balance}</td>
                </tr>
                <tr>
                  <td colSpan="2" className="text-center">
                    <Form.Group className="mb-3" controlId="formBasicPassword">
                      {/* <Form.Label>Amount</Form.Label> */}
                      <Form.Control
                        type="text"
                        placeholder="Electricity number"
                        name="phone"
                        value={phone}
                        onChange={(e) => setPhone(e.target.value)}
                      />
                    </Form.Group>
                    <Form.Group className="mb-3" controlId="formBasicPassword">
                      {/* <Form.Label>Amount</Form.Label> */}
                      <Form.Control
                        type="number"
                        min="0"
                        placeholder="Amount"
                        name="amount"
                        value={amount}
                        onChange={(e) => setAmount(e.target.value)}
                      />
                    </Form.Group>
                    <Button onClick={onIsShow} variant="primary">
                      Confirm
                    </Button>{" "}
                    <Button variant="warning" onClick={onClearAll}>
                      Clear all
                    </Button>
                  </td>
                </tr>
              </table>
            </div>
            {/* </div>
          <div className="d-flex flex-sm-column w-100"> */}
            <div
              style={{
                border: "1px solid lightgray",
                padding: "20px",
                width: "100%",
                margin: "0px auto",
                display: `${id == 2 ? "inline" : "none"}`,
              }}
            >
              <table
                style={{
                  //   border: "1px solid lightgray",
                  margin: "0 auto",
                  //   marginTop: "20px",
                  textAlign: "left",
                }}
              >
                <tr>
                  <td>Account Name:</td>
                  <td className="text-1">{user.name}</td>
                  <td rowSpan="6" className="text-center">
                    <Button variant="warning" onClick={() => setAmount(5)}>
                      5 $
                    </Button>{" "}
                    <Button variant="warning" onClick={() => setAmount(10)}>
                      10 $
                    </Button>{" "}
                    <br />
                    <Button
                      className="my-2"
                      variant="warning"
                      onClick={() => setAmount(1)}
                    >
                      1 $
                    </Button>{" "}
                    <Button
                      className="my-2"
                      variant="warning"
                      onClick={() => setAmount(2)}
                    >
                      2 $
                    </Button>
                  </td>
                  {/* <td rowSpan="6">Close</td> */}
                </tr>
                <tr>
                  <td>Account Number:</td>
                  <td className="text-1">{user.account_no}</td>
                </tr>
                <tr>
                  <td>Your Balance:</td>
                  <td className="text-1">{user.balance}</td>
                </tr>
                <tr>
                  <td colSpan="2" className="text-center">
                    <Form.Group className="mb-3" controlId="formBasicPassword">
                      {/* <Form.Label>Amount</Form.Label> */}
                      <Form.Control
                        type="text"
                        placeholder="Water number"
                        name="phone"
                        value={phone}
                        onChange={(e) => setPhone(e.target.value)}
                      />
                    </Form.Group>
                    <Form.Group className="mb-3" controlId="formBasicPassword">
                      {/* <Form.Label>Amount</Form.Label> */}
                      <Form.Control
                        type="number"
                        min="0"
                        placeholder="Amount"
                        name="amount"
                        value={amount}
                        onChange={(e) => setAmount(e.target.value)}
                      />
                    </Form.Group>
                    <Button onClick={onIsShow} variant="primary">
                      Confirm
                    </Button>{" "}
                    <Button variant="warning" onClick={onClearAll}>
                      Clear all
                    </Button>
                  </td>
                </tr>
              </table>
            </div>
          </div>

          <ModalReport
            isShow={isShow}
            titles={titles}
            descriptions={descriptions}
            onIsClose={onIsClose}
            onYes={onYes}
          />
        </Form>
      </div>
    </div>
  );
}
