import React from "react";
import { Button, Form } from "react-bootstrap";
import { useHistory } from "react-router-dom";
import "../App.css";

export default function Exit() {
  const history = useHistory();

  function onCloseAppTab(e) {
    // e.preventDefault();
    // window.open("about:blank", "_self");
    // window.close();
  }

  return (
    <div>
      <Button
        style={{ width: "400px" }}
        variant="success"
        className="mt-5 text-center"
        onClick={() => history.push('/menu')}
      >
        <table style={{ marginRight: "20px" }}>
          <tr>
            <td style={{ padding: "0" }}>
              <img
                style={{ marginTop: "-4px" }}
                width="20px"
                src="/images/back2.png"
              />
            </td>
            <td style={{ width: "100%", padding: "0" }}>
              <span>Exit</span>
            </td>
          </tr>
        </table>
      </Button>
      <div
        style={{
          border: "1px solid lightgray",
          padding: "20px",
          width: "640px",
          margin: "20px auto",
        }}
      >
        <Form>
          <h5>Do you want to exit?</h5>
          <Button
            onClick={onCloseAppTab}
            variant="primary"
            className="w-25"
            type="submit"
          >
            Yes
          </Button>{" "}
          <Button variant="warning" className="w-25" type="submit">
            No
          </Button>{" "}
        </Form>
      </div>
    </div>
  );
}
