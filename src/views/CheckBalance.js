import React, { useContext, useEffect, useState } from "react";
import { Button, Form } from "react-bootstrap";
import { useHistory } from "react-router-dom";
import { API } from "../api/API";
import { myContext } from "../App";
import "../App.css";

export default function CheckBalance() {
  const [user, setUser] = useState({name: '', account_no: '', balance: ''})
  const {token} = JSON.parse(localStorage.getItem('user'));
  const history = useHistory();

 //console.log("USER:",  localStorage.getItem('user'));
 useEffect(()=>{
   API.get('check-balance', {headers: {Authorization: "Bearer "+token}}).then(result=>{
      console.log("RESULT:", result);
      setUser({
        name: result.data.data.name,
        account_no: result.data.data.account_no,
        balance: result.data.data.balance
      })
   }).catch(error=>{
     console.log("ERROR:", error);
   })
 },[token])

  return (
    <div>
      <Button
        style={{ width: "400px" }}
        variant="success"
        className="mt-5 text-center"
        onClick={()=>history.push('/menu')}
      >
        <table style={{ marginRight: "20px" }}>
          <tr>
            <td style={{ padding: "0" }}>
              <img
                style={{ marginTop: "-4px" }}
                width="20px"
                src="/images/back2.png"
              />
            </td>
            <td style={{ width: "100%", padding: "0" }}>
              <span>Check Balance</span>
            </td>
          </tr>
        </table>
      </Button>
      <div
        style={{
          border: "1px solid lightgray",
          padding: "20px",
          width: '640px',
          margin: '20px auto'
        }}
      >
        <table
          style={{
            //   border: "1px solid lightgray",
            margin: "0 auto",
            //   marginTop: "20px",
            textAlign: "left",
          }}
        >
          <tr>
            <td>Account Name:</td>
            <td className="text-1">{user.name}</td>
            <td rowSpan="6" className="text-center">
              Your Balance
              <div className="balance">
                <p className="inner-balance">{user.balance} $</p>
              </div>
              {/* <Button variant="warning">Print Receipt</Button> */}
            </td>
            {/* <td rowSpan="6">Close</td> */}
          </tr>
          <tr>
            <td>Account Number:</td>
            <td className="text-1">{user.account_no}</td>
          </tr>
          <tr>
            <td colSpan="2">
              <Button
                style={{ width: "280px" }}
                variant="outline-success text-left"
                onClick={()=>history.push('/menu/cashwithdrawal')}
              >
                <img
                  style={{ margin: "-6px 10px 0 30px" }}
                  width="28px"
                  src="/images/withdrawal.png"
                />
                Cash Withdrawal
              </Button>
            </td>
          </tr>
          <tr>
            <td colSpan="2">
              <Button
                style={{ width: "280px" }}
                variant="outline-success text-left"
                onClick={()=>history.push('/menu/cashtransfer')}
              >
                <img
                  style={{ margin: "-6px 10px 0 30px" }}
                  width="26px"
                  src="/images/transfer.png"
                />
                Cash Transfer
              </Button>
            </td>
          </tr>
          <tr>
            <td colSpan="2">
              <Button
                style={{ width: "280px" }}
                variant="outline-success text-left"
                onClick={()=>history.push('/menu/billpayment')}
              >
                <img
                  style={{ margin: "-6px 10px 0 30px" }}
                  width="26px"
                  src="/images/payment.png"
                />
                Bill Payment
              </Button>
            </td>
          </tr>
          <tr>
            <td colSpan="2">
              <Button
                style={{ width: "280px", marginBottom: "10px" }}
                variant="outline-success text-left"
                onClick={()=>history.push('/menu/cashdeposit')}
              >
                <img
                  style={{ margin: "-6px 10px 0 30px" }}
                  width="24px"
                  src="/images/deposit.png"
                />
                Cash Deposit
              </Button>
            </td>
          </tr>
        </table>
      </div>
    </div>
  );
}
