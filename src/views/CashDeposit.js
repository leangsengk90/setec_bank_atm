import axios from "axios";
import React, { useContext, useState, useEffect } from "react";
import { Button, Form } from "react-bootstrap";
import { useHistory } from "react-router-dom";
import Swal from "sweetalert2";
import { API } from "../api/API";
import { myContext } from "../App";
import "../App.css";
import ModalReport from "../components/ModalReport";

export default function CashDeposit() {
  const [user, setUser] = useState({ name: "", account_no: "", balance: 0 });
  const [amount, setAmount] = useState(0);
  const { token } = JSON.parse(localStorage.getItem("user"));
  const history = useHistory();
  const [isShow, setIsShow] = useState(false);
  const [titles, setTitles] = useState([
    "Account Name",
    "Account Number",
    "Cash Deposit",
    "Total Balance",
    "Date",
  ]);

  const [descriptions, setDescriptions] = useState([
    "Kao Leangseng",
    "1234567890",
    100,
    600,
    "01-01-21",
  ]);

  function onIsShow() {
    if (amount > 0) {
      setDescriptions([
        user.name,
        user.account_no,
        amount,
        Number.parseFloat(user.balance) + Number.parseFloat(amount),
        new Date().toLocaleString(),
      ]);
      setIsShow(true);
    } else {
      Swal.fire({
        title: "Number Invalid",
        text: "Please input amount greater than 0!",
        icon: "warning",
        showCancelButton: true,
        confirmButtonColor: "#3085d6",
        cancelButtonColor: "#d33",
        confirmButtonText: "OK",
      }).then((result) => {
        // if (result.isConfirmed) {
        //   Swal.fire("OK", "Your file has been deleted.", "success");
        // }
      });
    }
  }

  function onYes() {
    API.post(
      `deposit?balance=${amount}`,
      {},
      { headers: { Authorization: "Bearer " + token } }
    )
      .then((result) => {
        console.log("RESULT:", result);
        setUser((curr) => ({
          ...curr,
          balance: Number.parseFloat(curr.balance) + Number.parseFloat(amount),
        }));
        setAmount(0);
      })
      .catch((error) => {
        console.log("ERROR:", error.responseText);
      });
    setIsShow(false);
  }

  function onIsClose() {
    setIsShow(false);
  }

  useEffect(() => {
    API.get("check-balance", { headers: { Authorization: "Bearer " + token } })
      .then((result) => {
        console.log("RESULT:", result);
        setUser({
          name: result.data.data.name,
          account_no: result.data.data.account_no,
          balance: result.data.data.balance,
        });
      })
      .catch((error) => {
        console.log("ERROR:", error);
      });
  }, [token]);

  return (
    <div>
      <Button
        style={{ width: "400px" }}
        variant="success"
        className="mt-5 text-center"
        onClick={() => history.push("/menu")}
      >
        <table style={{ marginRight: "20px" }}>
          <tr>
            <td style={{ padding: "0" }}>
              <img
                style={{ marginTop: "-4px" }}
                width="20px"
                src="/images/back2.png"
              />
            </td>
            <td style={{ width: "100%", padding: "0" }}>
              <span>Cash Deposit</span>
            </td>
          </tr>
        </table>
      </Button>
      <div
        style={{
          border: "1px solid lightgray",
          padding: "20px",
          width: "640px",
          margin: "20px auto",
        }}
      >
        <table
          style={{
            //   border: "1px solid lightgray",
            margin: "0 auto",
            //   marginTop: "20px",
            textAlign: "left",
          }}
        >
          <tr>
            <td>Account Name:</td>
            <td className="text-1">{user.name}</td>
            <td rowSpan="6" className="text-center">
              <Button variant="warning" onClick={() => setAmount(50)}>
                50 $
              </Button>{" "}
              <Button variant="warning" onClick={() => setAmount(100)}>
                100 $
              </Button>{" "}
              <br />
              <Button
                className="my-2"
                variant="warning"
                onClick={() => setAmount(10)}
              >
                10 $
              </Button>{" "}
              <Button
                className="my-2"
                variant="warning"
                onClick={() => setAmount(20)}
              >
                20 $
              </Button>
              <Form.Group className="mb-3" controlId="formBasicPassword">
                {/* <Form.Label>Amount</Form.Label> */}
                <Form.Control
                  type="number"
                  placeholder="Amount"
                  min="0"
                  name="amount"
                  value={amount}
                  onChange={(e) => setAmount(e.target.value)}
                />
              </Form.Group>
              <Button onClick={onIsShow} variant="primary">
                Confirm
              </Button>{" "}
              <Button variant="warning" onClick={() => setAmount(0)}>
                Clear
              </Button>
            </td>
            {/* <td rowSpan="6">Close</td> */}
          </tr>
          <tr>
            <td>Account Number:</td>
            <td className="text-1">{user.account_no}</td>
          </tr>
          <tr>
            <td>Your Balance:</td>
            <td className="text-1">{user.balance}</td>
          </tr>
          <tr>
            <td colSpan="2" className="text-center">
              <div className="balance mx-auto">
                <p className="inner-balance">{amount} $</p>
              </div>
              Cash in
            </td>
          </tr>
        </table>
      </div>
      <ModalReport
        isShow={isShow}
        titles={titles}
        descriptions={descriptions}
        onIsClose={onIsClose}
        onYes={onYes}
      />
    </div>
  );
}
