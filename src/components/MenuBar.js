import React, { useContext, useState } from "react";
import {
  Navbar,
  Nav,
  Form,
  FormControl,
  Button,
  Container,
} from "react-bootstrap";
import { myContext } from "../App";

export default function MenuBar() {
  const [isKhmer, setIsKhmer] = useState(true);
  const { setLang } = useContext(myContext);

  function onSetEnglish() {
    setIsKhmer(false);
    setLang((curr) => ({ ...curr, lidx: 0 }));
  }

  function onSetKhmer() {
    setIsKhmer(true);
    setLang((curr) => ({ ...curr, lidx: 1 }));
  }

  return (
    <Navbar bg="light" expand="lg">
      <Container>
        <Navbar.Brand href="/">
          <img
            style={{ marginTop: "-4px" }}
            width="40px"
            src="/images/bank.png"
          />
          <strong style={{ color: "rgb(65,65,65)" }}>SETEC Bank ATM</strong>
        </Navbar.Brand>
        <Navbar.Toggle aria-controls="navbarScroll" />
        <Navbar.Collapse id="navbarScroll">
          <Nav
            className="mr-auto my-2 my-lg-0"
            style={{ maxHeight: "100px" }}
            navbarScroll
          >
            {/* <Nav.Link href="#action1">Home</Nav.Link>
          <Nav.Link href="#action2">Link</Nav.Link>
          <Nav.Link href="#" disabled>
            Link
          </Nav.Link> */}
          </Nav>
          <Form className="d-flex">
            {/* <FormControl
            type="search"
            placeholder="Search"
            className="mr-2"
            aria-label="Search"
          /> */}
            {/* <span style={{color: 'green', marginTop: '6px', marginRight: '10px'}}>ខ្មែរ</span> */}
            <Button
              onClick={onSetKhmer}
              style={{ border: isKhmer ? null : "none", marginRight: "4px" }}
              variant="outline-success"
            >
              ខ្មែរ
            </Button>{" "}
            <Button
              onClick={onSetEnglish}
              variant="outline-success"
              style={{ border: isKhmer ? "none" : null }}
            >
              English
            </Button>
          </Form>
        </Navbar.Collapse>
      </Container>
    </Navbar>
  );
}
