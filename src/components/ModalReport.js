import React, { useState } from "react";
import { Modal, Button, Table } from "react-bootstrap";

export default function ModalReport(props) {
  return (
    <div>
      {/* <Button variant="primary" onClick={handleShow}>
        Launch demo modal
      </Button> */}

      <Modal show={props.isShow} onHide={props.onIsClose}>
        <Modal.Header closeButton>
          <Modal.Title style={{ color: "green" }}>
            Are you sure?
          </Modal.Title>
        </Modal.Header>
        <Modal.Body>
          <table>
              {props.titles.map((item, index) => (
                <tr key={index}>
                  <td>{item}:</td>
                  <td style={{ color: "green", fontWeight: "bold" }}>
                    {props.descriptions[index]}
                  </td>
                </tr>
              ))}
          </table>
        </Modal.Body>
        <Modal.Footer>
          <Button variant="primary" onClick={props.onYes}>
            Yes
          </Button>
          <Button variant="warning" onClick={props.onIsClose}>
            No
          </Button>
        </Modal.Footer>
      </Modal>
    </div>
  );
}
