import "./App.css";
import { BrowserRouter, Switch, Route } from "react-router-dom";
import Login from "./views/Login";
import MenuBar from "./components/MenuBar";
import "bootstrap/dist/css/bootstrap.min.css";
import { createContext, useState } from "react";
import MainMenu from "./views/MainMenu";
import CheckBalance from "./views/CheckBalance";
import ChangePassword from "./views/ChangePassword";
import Exit from "./views/Exit";
import TransactionReport from "./views/TransactionReport";
import CashDeposit from "./views/CashDeposit";
import CashWithdrawal from "./views/CashWithdrawal";
import CashTransfer from "./views/CashTransfer";
import BillPayment from "./views/BillPayment";
import SignUp from "./views/SignUp";
import useLang from "./customs/useLang";

export const myContext = createContext();

function App() {

  // const [lang, setLang] = useState({
  //   index: 1,
  //   signup: ['Sign up', 'ចុះឈ្មោះ'],  
  //   signin: ['Sign in', 'ចូល'], 
  //   welcome: ['WELCOME', 'សូមស្វាគមន៍'],
  //   username: ['Username', 'ឈ្មោះអ្នកប្រើប្រាស់'],
  //   password: ['Password', 'លេខសម្ងាត់'] 
  // })
  const [lang, setLang, user, setUser] = useLang();


  return (
    <div className="App">
    <myContext.Provider value={{user, setUser, lang, setLang}}>
      <BrowserRouter>
        <MenuBar />
        <Switch>
          <Route exact path="/" component={Login} />
          <Route exact path="/signup" component={SignUp} />
          <Route exact path="/menu" component={MainMenu} />
          <Route exact path="/menu/checkbalance" component={CheckBalance} />
          <Route exact path="/menu/changepassword" component={ChangePassword} />
          <Route exact path="/menu/exit" component={Exit} />
          <Route exact path="/menu/transactionreport" component={TransactionReport} />
          <Route exact path="/menu/cashdeposit" component={CashDeposit} />
          <Route exact path="/menu/cashwithdrawal" component={CashWithdrawal} />
          <Route exact path="/menu/cashtransfer" component={CashTransfer} />
          <Route exact path="/menu/billpayment" component={BillPayment} />
        </Switch>
      </BrowserRouter>
      </myContext.Provider>
    </div>
  );
}

export default App;
