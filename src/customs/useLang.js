import React, {useState} from 'react'

export default function useLang() {
  const [user, setUser] = useState([
    {
      name: "",
      username: "",
      password: "",
      account_no: "",
      balance: ""
    }
  ])


    //index: 1 => Khmer; index: 0 => English
    const [lang, setLang] = useState({
        lidx: 1,
        signup: ['Sign up', 'ចុះឈ្មោះ'],  
        signin: ['Sign in', 'ចូល'], 
        welcome: ['WELCOME', 'សូមស្វាគមន៍'],
        username: ['Username', 'ឈ្មោះអ្នកប្រើប្រាស់'],
        password: ['Password', 'លេខសម្ងាត់'] 
      })
    
    return [lang, setLang, user, setUser]
}
